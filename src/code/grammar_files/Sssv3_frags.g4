grammar Sssv3_frags;

// list of segments keywords for case insensitivity
METADATA   :   M E T A D A T A ;
PGXFORM    :   P G X F O R M ;
SSSCRITE   :   S S S C R I T E ;

// list of segtwo keywords for case insensitivity
LOC        :   L O C ;     // location of selector
COND       :   C O N D ;   // condition

// list Action related keywords in segtwo
ADD        :   A D D ;     
DEL        :   D E L ;     // delete
REPL       :   R E P L ;   // replace
MOD        :   M O D ;     // modify
PRCS       :   P R C S ;   // process
ADD_TT     :   A D D T T ;            //add-tt (for tooltip)
ADD_YT     :   A D D Y T ;// add-yt (for youtube)
ADD_IMG    :   A D D I M G ;// add-img (for img)
ADD_TXT    :   A D D T X T ; // add-txt (for text)
ADD_LINK   :   A D D L I N K ; // add-link (for hyperlink)
ADD_CSS    :   A D D C S S ;  // add-css (for a full css file)
ADD_STYLE  :   A D D S T Y L E ; // add-style (for style for a earmarked element)
ADD_HTML   :   A D D H T M L ; // add-html
ADD_SCRIPT :   A D D S C R I P T ; // add-script
REPL_LINK  :   R E P L L I N K ;  // repl-link
REPL_TXT   :   R E P L T X T ;  // repl-txt
REPL_IMG   :   R E P L I M G ;   // repl-img
MOD_ELM    :   M O D E L M ; // modify html tag related properties
REASMB     :   R E A S M B ; // Reassembly of a page

// list of seg1 attributes for case insensitivity
SSS_NAME    : S S S N A M E ;
SSS_ID      : S S S I D ;
SSS_DESC    : S S S D E S C ;
CURRATOR_NAME    : C U R R A T O R N A M E ;
CURRATOR_ID      : C U R R A T O R I D ;
RENURL      : R E N U R L ;  

// list of segtwo attributes(in loc) for case insensitivity
TAG             : T A G ;               // html tag based selection
NODE            : N O D E ;             // xPath based node selection
CSTAG           : C S T A G ;           // html tag like custom tag, which is user defined

// list of segtwo attributes(in action) for case insensitivity
SRC_LOC         : S R C L O C ;         // source location
NEW_TEXT        : N E W T E X T ;
ADD_LOC         : A D D L O C ;
FROM_TYPE       : F R O M T Y P E ; 
TO_TYPE         : T O T Y P E ;
TEXT            : T E X T ;    
LANG            : L A N G ;         // language to be used in translations
NUM_FM          : N U M F M ;       // format for numbering
CRNCY_FM        : C R N C Y F M ;   // currency format
TIME_FM         : T I M E F M ;     // format for time
UNIT_FM         : U N I T F M ;     // format for units of measure
COLOR           : C O L O R ;
CONV_RATE       : C O N V R A T E ;
ADD_CLASS       : A D D C L A S S ; // adds another class element to earmarked HTML tagged element 
MOD_CLASS       : M O D C L A S S ; // replaces the existing tag class with specified class 
MOD_ID          : M O D I D ;
// for use by custom event handlers
CSTM_DATA       : C S T M D A T A ;  
CSTM_SRC        : C S T M S R C ;    //script to be run as event handler
CSTM_HNDLR_NAME : C S T M H N D L R N A M E ;    // name to invoke


// list of seg3 attributes for case insensitivity
USR_NAME : U S R N A M E ;
USR_ID : U S R I D ;
CMNTY_NAME  : C M N T Y N A M E ;  // community name
CMNTY_ID    : C M N T Y I D ;      // community identification
TOD : T O D ;
BROWSER : B R O W S E R ;

// related to STRINGS
ESC        : '\\' (["\\/bfnrt] | UNICODE) ;
UNICODE    : 'u' HEX HEX HEX HEX ;
HEX        : [0-9a-fA-F] ;
INT        : '0' | [1-9] [0-9]* ; // no leading zeros
EXP        : [Ee] [+\-]? INT ;

// list of alphabets for case insensitivity
fragment A  : 'a' | 'A' ;
fragment B  : 'b' | 'B' ;
fragment C  : 'c' | 'C' ;
fragment D  : 'd' | 'D' ;
fragment E  : 'e' | 'E' ;
fragment F  : 'f' | 'F' ;
fragment G  : 'g' | 'G' ;
fragment H  : 'h' | 'H' ;
fragment I  : 'i' | 'I' ;
fragment J  : 'j' | 'J' ;
fragment K  : 'k' | 'K' ;
fragment L  : 'l' | 'L' ;
fragment M  : 'm' | 'M' ;
fragment N  : 'n' | 'N' ;
fragment O  : 'o' | 'O' ;
fragment P  : 'p' | 'P' ;
fragment Q  : 'q' | 'Q' ;
fragment R  : 'r' | 'R' ;
fragment S  : 's' | 'S' ;
fragment T  : 't' | 'T' ;
fragment U  : 'u' | 'U' ;
fragment V  : 'v' | 'V' ;
fragment W  : 'w' | 'W' ;
fragment X  : 'x' | 'X' ;
fragment Y  : 'y' | 'Y' ;
fragment Z  : 'z' | 'Z' ;

