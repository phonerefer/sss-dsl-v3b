grammar Sssv3_pars;
import  Sssv3_lex;
// Parser Rules

// a SSS consists of 3 segments seg1, 2, 3
// s1 is metadata; s2 page transform statments, s3 is about sss selection criteria
// sss = s1, s2, s3
sss
   : (s1_metadata ',' s2_pgxform_stmnts ',' s3_ssscrite)
   | (s1_metadata ',' s2_pgxform_stmnts)
   | EOF
   ;

// segment 1: metadata
// mdav - metadata attribute value pairs
// metadata {SSS_ID:123, SSS_NAME:"xyz",...}
s1_metadata
   : Meta_data '{' md_avp '}'  
   ;

// md_avp - attribute value pairs
md_avp
   : Attr ':' Value (',' ( Attr ':' Value ) )*
   ;

// segment 2
// pgxform{(stmnts),(),()}
s2_pgxform_stmnts
   : Pg_xform '{' '(' stmnt ')' ( ',' '(' stmnt ')' )*  '}'              
   ;

// loc(tag="p"):{(add:(), cond(who="Sai")), (add:(), cond(who=""))}
stmnt
   : Loc '(' loc_avp ')' ':' '{' '(' ac_pair ')' (',' '(' ac_pair ')')* '}'
   ;

ac_pair
   :  Action ':' '(' actn_avp ')' ',' Cond ':' '(' cond_avp ')' 
   // When no condition is given action applies to all
   |  Action ':' '(' actn_avp ')' 
   ;

// loc_avp - attribute value pairs
loc_avp
   : Attr ':' Value (',' ( Attr ':' Value ) )*
   ;

// actn_avp - attribute value pairs
actn_avp
   : Attr ':' Value (',' ( Attr ':' Value ) )*
   ;

// Cond_avp - attribute value pairs
cond_avp
   : Attr ':' Value (',' ( Attr ':' Value ) )*
   ;

// seg3 - selection criteria for choosing one SSS over others
s3_ssscrite
   : Sss_crite '{' selctn_avp '}'  
   ;

// selctn_avp - attribute value pairs
selctn_avp
   : Attr ':' Value (',' ( Attr ':' Value ) )*
   ;