import sys
from antlr4 import *
from Sssv3Lexer import Sssv3Lexer
from Sssv3Parser import Sssv3Parser
from listen import Listen

def main(argv):
   input = FileStream(argv[1])
   lexer = Sssv3Lexer(input)
   stream = CommonTokenStream(lexer)
   parser = Sssv3Parser(stream)
   tree = parser.sss()
   #print(tree.toStringTree(recog=parser))
   walker = ParseTreeWalker()
   walker.walk(Listen(), tree)

if __name__ == '__main__':
   main(sys.argv)