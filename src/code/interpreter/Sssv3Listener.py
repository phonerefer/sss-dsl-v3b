# Generated from Sssv3.g4 by ANTLR 4.7
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .Sssv3Parser import Sssv3Parser
else:
    from Sssv3Parser import Sssv3Parser

# This class defines a complete listener for a parse tree produced by Sssv3Parser.
class Sssv3Listener(ParseTreeListener):

    # Enter a parse tree produced by Sssv3Parser#sss.
    def enterSss(self, ctx:Sssv3Parser.SssContext):
        pass

    # Exit a parse tree produced by Sssv3Parser#sss.
    def exitSss(self, ctx:Sssv3Parser.SssContext):
        pass


    # Enter a parse tree produced by Sssv3Parser#s1_metadata.
    def enterS1_metadata(self, ctx:Sssv3Parser.S1_metadataContext):
        pass

    # Exit a parse tree produced by Sssv3Parser#s1_metadata.
    def exitS1_metadata(self, ctx:Sssv3Parser.S1_metadataContext):
        pass


    # Enter a parse tree produced by Sssv3Parser#md_avp.
    def enterMd_avp(self, ctx:Sssv3Parser.Md_avpContext):
        pass

    # Exit a parse tree produced by Sssv3Parser#md_avp.
    def exitMd_avp(self, ctx:Sssv3Parser.Md_avpContext):
        pass


    # Enter a parse tree produced by Sssv3Parser#s2_pgxform_stmnts.
    def enterS2_pgxform_stmnts(self, ctx:Sssv3Parser.S2_pgxform_stmntsContext):
        pass

    # Exit a parse tree produced by Sssv3Parser#s2_pgxform_stmnts.
    def exitS2_pgxform_stmnts(self, ctx:Sssv3Parser.S2_pgxform_stmntsContext):
        pass


    # Enter a parse tree produced by Sssv3Parser#stmnt.
    def enterStmnt(self, ctx:Sssv3Parser.StmntContext):
        pass

    # Exit a parse tree produced by Sssv3Parser#stmnt.
    def exitStmnt(self, ctx:Sssv3Parser.StmntContext):
        pass


    # Enter a parse tree produced by Sssv3Parser#ac_pair.
    def enterAc_pair(self, ctx:Sssv3Parser.Ac_pairContext):
        pass

    # Exit a parse tree produced by Sssv3Parser#ac_pair.
    def exitAc_pair(self, ctx:Sssv3Parser.Ac_pairContext):
        pass


    # Enter a parse tree produced by Sssv3Parser#loc_avp.
    def enterLoc_avp(self, ctx:Sssv3Parser.Loc_avpContext):
        pass

    # Exit a parse tree produced by Sssv3Parser#loc_avp.
    def exitLoc_avp(self, ctx:Sssv3Parser.Loc_avpContext):
        pass


    # Enter a parse tree produced by Sssv3Parser#actn_avp.
    def enterActn_avp(self, ctx:Sssv3Parser.Actn_avpContext):
        pass

    # Exit a parse tree produced by Sssv3Parser#actn_avp.
    def exitActn_avp(self, ctx:Sssv3Parser.Actn_avpContext):
        pass


    # Enter a parse tree produced by Sssv3Parser#cond_avp.
    def enterCond_avp(self, ctx:Sssv3Parser.Cond_avpContext):
        pass

    # Exit a parse tree produced by Sssv3Parser#cond_avp.
    def exitCond_avp(self, ctx:Sssv3Parser.Cond_avpContext):
        pass


    # Enter a parse tree produced by Sssv3Parser#s3_ssscrite.
    def enterS3_ssscrite(self, ctx:Sssv3Parser.S3_ssscriteContext):
        pass

    # Exit a parse tree produced by Sssv3Parser#s3_ssscrite.
    def exitS3_ssscrite(self, ctx:Sssv3Parser.S3_ssscriteContext):
        pass


    # Enter a parse tree produced by Sssv3Parser#selctn_avp.
    def enterSelctn_avp(self, ctx:Sssv3Parser.Selctn_avpContext):
        pass

    # Exit a parse tree produced by Sssv3Parser#selctn_avp.
    def exitSelctn_avp(self, ctx:Sssv3Parser.Selctn_avpContext):
        pass


