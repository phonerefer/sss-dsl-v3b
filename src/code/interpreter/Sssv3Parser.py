# Generated from Sssv3.g4 by ANTLR 4.7
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3S")
        buf.write("\u00a6\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\3\2\3\2\3\2")
        buf.write("\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\5\2$\n\2\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\7\4\62\n\4\f\4\16")
        buf.write("\4\65\13\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\7\5")
        buf.write("A\n\5\f\5\16\5D\13\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3")
        buf.write("\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\7\6V\n\6\f\6\16\6Y\13\6")
        buf.write("\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3")
        buf.write("\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7o\n\7\3\b\3\b\3\b\3\b\3")
        buf.write("\b\3\b\3\b\7\bx\n\b\f\b\16\b{\13\b\3\t\3\t\3\t\3\t\3\t")
        buf.write("\3\t\3\t\7\t\u0084\n\t\f\t\16\t\u0087\13\t\3\n\3\n\3\n")
        buf.write("\3\n\3\n\3\n\3\n\7\n\u0090\n\n\f\n\16\n\u0093\13\n\3\13")
        buf.write("\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\7\f\u00a1")
        buf.write("\n\f\f\f\16\f\u00a4\13\f\3\f\2\2\r\2\4\6\b\n\f\16\20\22")
        buf.write("\24\26\2\2\2\u00a4\2#\3\2\2\2\4%\3\2\2\2\6*\3\2\2\2\b")
        buf.write("\66\3\2\2\2\nG\3\2\2\2\fn\3\2\2\2\16p\3\2\2\2\20|\3\2")
        buf.write("\2\2\22\u0088\3\2\2\2\24\u0094\3\2\2\2\26\u0099\3\2\2")
        buf.write("\2\30\31\5\4\3\2\31\32\7\3\2\2\32\33\5\b\5\2\33\34\7\3")
        buf.write("\2\2\34\35\5\24\13\2\35$\3\2\2\2\36\37\5\4\3\2\37 \7\3")
        buf.write("\2\2 !\5\b\5\2!$\3\2\2\2\"$\7\2\2\3#\30\3\2\2\2#\36\3")
        buf.write("\2\2\2#\"\3\2\2\2$\3\3\2\2\2%&\7\t\2\2&\'\7\4\2\2\'(\5")
        buf.write("\6\4\2()\7\5\2\2)\5\3\2\2\2*+\7\17\2\2+,\7\6\2\2,\63\7")
        buf.write("\20\2\2-.\7\3\2\2./\7\17\2\2/\60\7\6\2\2\60\62\7\20\2")
        buf.write("\2\61-\3\2\2\2\62\65\3\2\2\2\63\61\3\2\2\2\63\64\3\2\2")
        buf.write("\2\64\7\3\2\2\2\65\63\3\2\2\2\66\67\7\n\2\2\678\7\4\2")
        buf.write("\289\7\7\2\29:\5\n\6\2:B\7\b\2\2;<\7\3\2\2<=\7\7\2\2=")
        buf.write(">\5\n\6\2>?\7\b\2\2?A\3\2\2\2@;\3\2\2\2AD\3\2\2\2B@\3")
        buf.write("\2\2\2BC\3\2\2\2CE\3\2\2\2DB\3\2\2\2EF\7\5\2\2F\t\3\2")
        buf.write("\2\2GH\7\13\2\2HI\7\7\2\2IJ\5\16\b\2JK\7\b\2\2KL\7\6\2")
        buf.write("\2LM\7\4\2\2MN\7\7\2\2NO\5\f\7\2OW\7\b\2\2PQ\7\3\2\2Q")
        buf.write("R\7\7\2\2RS\5\f\7\2ST\7\b\2\2TV\3\2\2\2UP\3\2\2\2VY\3")
        buf.write("\2\2\2WU\3\2\2\2WX\3\2\2\2XZ\3\2\2\2YW\3\2\2\2Z[\7\5\2")
        buf.write("\2[\13\3\2\2\2\\]\7\f\2\2]^\7\6\2\2^_\7\7\2\2_`\5\20\t")
        buf.write("\2`a\7\b\2\2ab\7\3\2\2bc\7\r\2\2cd\7\6\2\2de\7\7\2\2e")
        buf.write("f\5\22\n\2fg\7\b\2\2go\3\2\2\2hi\7\f\2\2ij\7\6\2\2jk\7")
        buf.write("\7\2\2kl\5\20\t\2lm\7\b\2\2mo\3\2\2\2n\\\3\2\2\2nh\3\2")
        buf.write("\2\2o\r\3\2\2\2pq\7\17\2\2qr\7\6\2\2ry\7\20\2\2st\7\3")
        buf.write("\2\2tu\7\17\2\2uv\7\6\2\2vx\7\20\2\2ws\3\2\2\2x{\3\2\2")
        buf.write("\2yw\3\2\2\2yz\3\2\2\2z\17\3\2\2\2{y\3\2\2\2|}\7\17\2")
        buf.write("\2}~\7\6\2\2~\u0085\7\20\2\2\177\u0080\7\3\2\2\u0080\u0081")
        buf.write("\7\17\2\2\u0081\u0082\7\6\2\2\u0082\u0084\7\20\2\2\u0083")
        buf.write("\177\3\2\2\2\u0084\u0087\3\2\2\2\u0085\u0083\3\2\2\2\u0085")
        buf.write("\u0086\3\2\2\2\u0086\21\3\2\2\2\u0087\u0085\3\2\2\2\u0088")
        buf.write("\u0089\7\17\2\2\u0089\u008a\7\6\2\2\u008a\u0091\7\20\2")
        buf.write("\2\u008b\u008c\7\3\2\2\u008c\u008d\7\17\2\2\u008d\u008e")
        buf.write("\7\6\2\2\u008e\u0090\7\20\2\2\u008f\u008b\3\2\2\2\u0090")
        buf.write("\u0093\3\2\2\2\u0091\u008f\3\2\2\2\u0091\u0092\3\2\2\2")
        buf.write("\u0092\23\3\2\2\2\u0093\u0091\3\2\2\2\u0094\u0095\7\16")
        buf.write("\2\2\u0095\u0096\7\4\2\2\u0096\u0097\5\26\f\2\u0097\u0098")
        buf.write("\7\5\2\2\u0098\25\3\2\2\2\u0099\u009a\7\17\2\2\u009a\u009b")
        buf.write("\7\6\2\2\u009b\u00a2\7\20\2\2\u009c\u009d\7\3\2\2\u009d")
        buf.write("\u009e\7\17\2\2\u009e\u009f\7\6\2\2\u009f\u00a1\7\20\2")
        buf.write("\2\u00a0\u009c\3\2\2\2\u00a1\u00a4\3\2\2\2\u00a2\u00a0")
        buf.write("\3\2\2\2\u00a2\u00a3\3\2\2\2\u00a3\27\3\2\2\2\u00a4\u00a2")
        buf.write("\3\2\2\2\13#\63BWny\u0085\u0091\u00a2")
        return buf.getvalue()


class Sssv3Parser ( Parser ):

    grammarFileName = "Sssv3.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "','", "'{'", "'}'", "':'", "'('", "')'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "Meta_data", 
                      "Pg_xform", "Loc", "Action", "Cond", "Sss_crite", 
                      "Attr", "Value", "STRING", "NUMBER", "WS", "LINE_COMMENT", 
                      "METADATA", "PGXFORM", "SSSCRITE", "LOC", "COND", 
                      "ADD", "DEL", "REPL", "MOD", "PRCS", "ADD_TT", "ADD_YT", 
                      "ADD_IMG", "ADD_TXT", "ADD_LINK", "ADD_CSS", "ADD_STYLE", 
                      "ADD_HTML", "ADD_SCRIPT", "REPL_LINK", "REPL_TXT", 
                      "REPL_IMG", "MOD_ELM", "REASMB", "SSS_NAME", "SSS_ID", 
                      "SSS_DESC", "CURRATOR_NAME", "CURRATOR_ID", "RENURL", 
                      "TAG", "NODE", "CSTAG", "SRC_LOC", "NEW_TEXT", "ADD_LOC", 
                      "FROM_TYPE", "TO_TYPE", "TEXT", "LANG", "NUM_FM", 
                      "CRNCY_FM", "TIME_FM", "UNIT_FM", "COLOR", "CONV_RATE", 
                      "ADD_CLASS", "MOD_CLASS", "MOD_ID", "CSTM_DATA", "CSTM_SRC", 
                      "CSTM_HNDLR_NAME", "USR_NAME", "USR_ID", "CMNTY_NAME", 
                      "CMNTY_ID", "TOD", "BROWSER", "ESC", "UNICODE", "HEX", 
                      "INT", "EXP" ]

    RULE_sss = 0
    RULE_s1_metadata = 1
    RULE_md_avp = 2
    RULE_s2_pgxform_stmnts = 3
    RULE_stmnt = 4
    RULE_ac_pair = 5
    RULE_loc_avp = 6
    RULE_actn_avp = 7
    RULE_cond_avp = 8
    RULE_s3_ssscrite = 9
    RULE_selctn_avp = 10

    ruleNames =  [ "sss", "s1_metadata", "md_avp", "s2_pgxform_stmnts", 
                   "stmnt", "ac_pair", "loc_avp", "actn_avp", "cond_avp", 
                   "s3_ssscrite", "selctn_avp" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    Meta_data=7
    Pg_xform=8
    Loc=9
    Action=10
    Cond=11
    Sss_crite=12
    Attr=13
    Value=14
    STRING=15
    NUMBER=16
    WS=17
    LINE_COMMENT=18
    METADATA=19
    PGXFORM=20
    SSSCRITE=21
    LOC=22
    COND=23
    ADD=24
    DEL=25
    REPL=26
    MOD=27
    PRCS=28
    ADD_TT=29
    ADD_YT=30
    ADD_IMG=31
    ADD_TXT=32
    ADD_LINK=33
    ADD_CSS=34
    ADD_STYLE=35
    ADD_HTML=36
    ADD_SCRIPT=37
    REPL_LINK=38
    REPL_TXT=39
    REPL_IMG=40
    MOD_ELM=41
    REASMB=42
    SSS_NAME=43
    SSS_ID=44
    SSS_DESC=45
    CURRATOR_NAME=46
    CURRATOR_ID=47
    RENURL=48
    TAG=49
    NODE=50
    CSTAG=51
    SRC_LOC=52
    NEW_TEXT=53
    ADD_LOC=54
    FROM_TYPE=55
    TO_TYPE=56
    TEXT=57
    LANG=58
    NUM_FM=59
    CRNCY_FM=60
    TIME_FM=61
    UNIT_FM=62
    COLOR=63
    CONV_RATE=64
    ADD_CLASS=65
    MOD_CLASS=66
    MOD_ID=67
    CSTM_DATA=68
    CSTM_SRC=69
    CSTM_HNDLR_NAME=70
    USR_NAME=71
    USR_ID=72
    CMNTY_NAME=73
    CMNTY_ID=74
    TOD=75
    BROWSER=76
    ESC=77
    UNICODE=78
    HEX=79
    INT=80
    EXP=81

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class SssContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def s1_metadata(self):
            return self.getTypedRuleContext(Sssv3Parser.S1_metadataContext,0)


        def s2_pgxform_stmnts(self):
            return self.getTypedRuleContext(Sssv3Parser.S2_pgxform_stmntsContext,0)


        def s3_ssscrite(self):
            return self.getTypedRuleContext(Sssv3Parser.S3_ssscriteContext,0)


        def EOF(self):
            return self.getToken(Sssv3Parser.EOF, 0)

        def getRuleIndex(self):
            return Sssv3Parser.RULE_sss

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSss" ):
                listener.enterSss(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSss" ):
                listener.exitSss(self)




    def sss(self):

        localctx = Sssv3Parser.SssContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_sss)
        try:
            self.state = 33
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,0,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 22
                self.s1_metadata()
                self.state = 23
                self.match(Sssv3Parser.T__0)
                self.state = 24
                self.s2_pgxform_stmnts()
                self.state = 25
                self.match(Sssv3Parser.T__0)
                self.state = 26
                self.s3_ssscrite()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 28
                self.s1_metadata()
                self.state = 29
                self.match(Sssv3Parser.T__0)
                self.state = 30
                self.s2_pgxform_stmnts()
                pass

            elif la_ == 3:
                self.enterOuterAlt(localctx, 3)
                self.state = 32
                self.match(Sssv3Parser.EOF)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class S1_metadataContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Meta_data(self):
            return self.getToken(Sssv3Parser.Meta_data, 0)

        def md_avp(self):
            return self.getTypedRuleContext(Sssv3Parser.Md_avpContext,0)


        def getRuleIndex(self):
            return Sssv3Parser.RULE_s1_metadata

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterS1_metadata" ):
                listener.enterS1_metadata(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitS1_metadata" ):
                listener.exitS1_metadata(self)




    def s1_metadata(self):

        localctx = Sssv3Parser.S1_metadataContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_s1_metadata)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 35
            self.match(Sssv3Parser.Meta_data)
            self.state = 36
            self.match(Sssv3Parser.T__1)
            self.state = 37
            self.md_avp()
            self.state = 38
            self.match(Sssv3Parser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Md_avpContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Attr(self, i:int=None):
            if i is None:
                return self.getTokens(Sssv3Parser.Attr)
            else:
                return self.getToken(Sssv3Parser.Attr, i)

        def Value(self, i:int=None):
            if i is None:
                return self.getTokens(Sssv3Parser.Value)
            else:
                return self.getToken(Sssv3Parser.Value, i)

        def getRuleIndex(self):
            return Sssv3Parser.RULE_md_avp

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMd_avp" ):
                listener.enterMd_avp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMd_avp" ):
                listener.exitMd_avp(self)




    def md_avp(self):

        localctx = Sssv3Parser.Md_avpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_md_avp)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 40
            self.match(Sssv3Parser.Attr)
            self.state = 41
            self.match(Sssv3Parser.T__3)
            self.state = 42
            self.match(Sssv3Parser.Value)
            self.state = 49
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Sssv3Parser.T__0:
                self.state = 43
                self.match(Sssv3Parser.T__0)

                self.state = 44
                self.match(Sssv3Parser.Attr)
                self.state = 45
                self.match(Sssv3Parser.T__3)
                self.state = 46
                self.match(Sssv3Parser.Value)
                self.state = 51
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class S2_pgxform_stmntsContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Pg_xform(self):
            return self.getToken(Sssv3Parser.Pg_xform, 0)

        def stmnt(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Sssv3Parser.StmntContext)
            else:
                return self.getTypedRuleContext(Sssv3Parser.StmntContext,i)


        def getRuleIndex(self):
            return Sssv3Parser.RULE_s2_pgxform_stmnts

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterS2_pgxform_stmnts" ):
                listener.enterS2_pgxform_stmnts(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitS2_pgxform_stmnts" ):
                listener.exitS2_pgxform_stmnts(self)




    def s2_pgxform_stmnts(self):

        localctx = Sssv3Parser.S2_pgxform_stmntsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_s2_pgxform_stmnts)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 52
            self.match(Sssv3Parser.Pg_xform)
            self.state = 53
            self.match(Sssv3Parser.T__1)
            self.state = 54
            self.match(Sssv3Parser.T__4)
            self.state = 55
            self.stmnt()
            self.state = 56
            self.match(Sssv3Parser.T__5)
            self.state = 64
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Sssv3Parser.T__0:
                self.state = 57
                self.match(Sssv3Parser.T__0)
                self.state = 58
                self.match(Sssv3Parser.T__4)
                self.state = 59
                self.stmnt()
                self.state = 60
                self.match(Sssv3Parser.T__5)
                self.state = 66
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 67
            self.match(Sssv3Parser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StmntContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Loc(self):
            return self.getToken(Sssv3Parser.Loc, 0)

        def loc_avp(self):
            return self.getTypedRuleContext(Sssv3Parser.Loc_avpContext,0)


        def ac_pair(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Sssv3Parser.Ac_pairContext)
            else:
                return self.getTypedRuleContext(Sssv3Parser.Ac_pairContext,i)


        def getRuleIndex(self):
            return Sssv3Parser.RULE_stmnt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStmnt" ):
                listener.enterStmnt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStmnt" ):
                listener.exitStmnt(self)




    def stmnt(self):

        localctx = Sssv3Parser.StmntContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_stmnt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 69
            self.match(Sssv3Parser.Loc)
            self.state = 70
            self.match(Sssv3Parser.T__4)
            self.state = 71
            self.loc_avp()
            self.state = 72
            self.match(Sssv3Parser.T__5)
            self.state = 73
            self.match(Sssv3Parser.T__3)
            self.state = 74
            self.match(Sssv3Parser.T__1)
            self.state = 75
            self.match(Sssv3Parser.T__4)
            self.state = 76
            self.ac_pair()
            self.state = 77
            self.match(Sssv3Parser.T__5)
            self.state = 85
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Sssv3Parser.T__0:
                self.state = 78
                self.match(Sssv3Parser.T__0)
                self.state = 79
                self.match(Sssv3Parser.T__4)
                self.state = 80
                self.ac_pair()
                self.state = 81
                self.match(Sssv3Parser.T__5)
                self.state = 87
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 88
            self.match(Sssv3Parser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Ac_pairContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Action(self):
            return self.getToken(Sssv3Parser.Action, 0)

        def actn_avp(self):
            return self.getTypedRuleContext(Sssv3Parser.Actn_avpContext,0)


        def Cond(self):
            return self.getToken(Sssv3Parser.Cond, 0)

        def cond_avp(self):
            return self.getTypedRuleContext(Sssv3Parser.Cond_avpContext,0)


        def getRuleIndex(self):
            return Sssv3Parser.RULE_ac_pair

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAc_pair" ):
                listener.enterAc_pair(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAc_pair" ):
                listener.exitAc_pair(self)




    def ac_pair(self):

        localctx = Sssv3Parser.Ac_pairContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_ac_pair)
        try:
            self.state = 108
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,4,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 90
                self.match(Sssv3Parser.Action)
                self.state = 91
                self.match(Sssv3Parser.T__3)
                self.state = 92
                self.match(Sssv3Parser.T__4)
                self.state = 93
                self.actn_avp()
                self.state = 94
                self.match(Sssv3Parser.T__5)
                self.state = 95
                self.match(Sssv3Parser.T__0)
                self.state = 96
                self.match(Sssv3Parser.Cond)
                self.state = 97
                self.match(Sssv3Parser.T__3)
                self.state = 98
                self.match(Sssv3Parser.T__4)
                self.state = 99
                self.cond_avp()
                self.state = 100
                self.match(Sssv3Parser.T__5)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 102
                self.match(Sssv3Parser.Action)
                self.state = 103
                self.match(Sssv3Parser.T__3)
                self.state = 104
                self.match(Sssv3Parser.T__4)
                self.state = 105
                self.actn_avp()
                self.state = 106
                self.match(Sssv3Parser.T__5)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Loc_avpContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Attr(self, i:int=None):
            if i is None:
                return self.getTokens(Sssv3Parser.Attr)
            else:
                return self.getToken(Sssv3Parser.Attr, i)

        def Value(self, i:int=None):
            if i is None:
                return self.getTokens(Sssv3Parser.Value)
            else:
                return self.getToken(Sssv3Parser.Value, i)

        def getRuleIndex(self):
            return Sssv3Parser.RULE_loc_avp

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLoc_avp" ):
                listener.enterLoc_avp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLoc_avp" ):
                listener.exitLoc_avp(self)




    def loc_avp(self):

        localctx = Sssv3Parser.Loc_avpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_loc_avp)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 110
            self.match(Sssv3Parser.Attr)
            self.state = 111
            self.match(Sssv3Parser.T__3)
            self.state = 112
            self.match(Sssv3Parser.Value)
            self.state = 119
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Sssv3Parser.T__0:
                self.state = 113
                self.match(Sssv3Parser.T__0)

                self.state = 114
                self.match(Sssv3Parser.Attr)
                self.state = 115
                self.match(Sssv3Parser.T__3)
                self.state = 116
                self.match(Sssv3Parser.Value)
                self.state = 121
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Actn_avpContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Attr(self, i:int=None):
            if i is None:
                return self.getTokens(Sssv3Parser.Attr)
            else:
                return self.getToken(Sssv3Parser.Attr, i)

        def Value(self, i:int=None):
            if i is None:
                return self.getTokens(Sssv3Parser.Value)
            else:
                return self.getToken(Sssv3Parser.Value, i)

        def getRuleIndex(self):
            return Sssv3Parser.RULE_actn_avp

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterActn_avp" ):
                listener.enterActn_avp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitActn_avp" ):
                listener.exitActn_avp(self)




    def actn_avp(self):

        localctx = Sssv3Parser.Actn_avpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_actn_avp)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 122
            self.match(Sssv3Parser.Attr)
            self.state = 123
            self.match(Sssv3Parser.T__3)
            self.state = 124
            self.match(Sssv3Parser.Value)
            self.state = 131
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Sssv3Parser.T__0:
                self.state = 125
                self.match(Sssv3Parser.T__0)

                self.state = 126
                self.match(Sssv3Parser.Attr)
                self.state = 127
                self.match(Sssv3Parser.T__3)
                self.state = 128
                self.match(Sssv3Parser.Value)
                self.state = 133
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Cond_avpContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Attr(self, i:int=None):
            if i is None:
                return self.getTokens(Sssv3Parser.Attr)
            else:
                return self.getToken(Sssv3Parser.Attr, i)

        def Value(self, i:int=None):
            if i is None:
                return self.getTokens(Sssv3Parser.Value)
            else:
                return self.getToken(Sssv3Parser.Value, i)

        def getRuleIndex(self):
            return Sssv3Parser.RULE_cond_avp

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCond_avp" ):
                listener.enterCond_avp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCond_avp" ):
                listener.exitCond_avp(self)




    def cond_avp(self):

        localctx = Sssv3Parser.Cond_avpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_cond_avp)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 134
            self.match(Sssv3Parser.Attr)
            self.state = 135
            self.match(Sssv3Parser.T__3)
            self.state = 136
            self.match(Sssv3Parser.Value)
            self.state = 143
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Sssv3Parser.T__0:
                self.state = 137
                self.match(Sssv3Parser.T__0)

                self.state = 138
                self.match(Sssv3Parser.Attr)
                self.state = 139
                self.match(Sssv3Parser.T__3)
                self.state = 140
                self.match(Sssv3Parser.Value)
                self.state = 145
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class S3_ssscriteContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Sss_crite(self):
            return self.getToken(Sssv3Parser.Sss_crite, 0)

        def selctn_avp(self):
            return self.getTypedRuleContext(Sssv3Parser.Selctn_avpContext,0)


        def getRuleIndex(self):
            return Sssv3Parser.RULE_s3_ssscrite

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterS3_ssscrite" ):
                listener.enterS3_ssscrite(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitS3_ssscrite" ):
                listener.exitS3_ssscrite(self)




    def s3_ssscrite(self):

        localctx = Sssv3Parser.S3_ssscriteContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_s3_ssscrite)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 146
            self.match(Sssv3Parser.Sss_crite)
            self.state = 147
            self.match(Sssv3Parser.T__1)
            self.state = 148
            self.selctn_avp()
            self.state = 149
            self.match(Sssv3Parser.T__2)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Selctn_avpContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def Attr(self, i:int=None):
            if i is None:
                return self.getTokens(Sssv3Parser.Attr)
            else:
                return self.getToken(Sssv3Parser.Attr, i)

        def Value(self, i:int=None):
            if i is None:
                return self.getTokens(Sssv3Parser.Value)
            else:
                return self.getToken(Sssv3Parser.Value, i)

        def getRuleIndex(self):
            return Sssv3Parser.RULE_selctn_avp

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSelctn_avp" ):
                listener.enterSelctn_avp(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSelctn_avp" ):
                listener.exitSelctn_avp(self)




    def selctn_avp(self):

        localctx = Sssv3Parser.Selctn_avpContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_selctn_avp)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 151
            self.match(Sssv3Parser.Attr)
            self.state = 152
            self.match(Sssv3Parser.T__3)
            self.state = 153
            self.match(Sssv3Parser.Value)
            self.state = 160
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Sssv3Parser.T__0:
                self.state = 154
                self.match(Sssv3Parser.T__0)

                self.state = 155
                self.match(Sssv3Parser.Attr)
                self.state = 156
                self.match(Sssv3Parser.T__3)
                self.state = 157
                self.match(Sssv3Parser.Value)
                self.state = 162
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





