# Generated from Sssv3.g4 by ANTLR 4.7
from antlr4 import *
from Sssv3Parser import Sssv3Parser
from Sssv3Parser import Sssv3Parser
from Sssv3Listener import Sssv3Listener
import sss_adm
import pprint

# This class defines a complete listener for a parse tree produced by Sssv3Parser.
class Listen(Sssv3Listener):

    # Enter a parse tree produced by Sssv3Parser#sss.
    def enterSss(self, ctx:Sssv3Parser.SssContext):
        """
        Instantiates class `SssModel` to `sss` object 
        """
        self.sss = sss_adm.SssModel()
        """
        Calls `setSss` method to create a `sss` dict. 
        self.sss = {}
        """
        self.sss.setSss()

    # Exit a parse tree produced by Sssv3Parser#sss.
    def exitSss(self, ctx:Sssv3Parser.SssContext):
        """
        Call method `getSss` which returns generated abstract data model
        """
        adm = self.sss.getSss()
        pprint.pprint(adm)
        """
        Validates sss "abstract data model"
        """
        sss_adm.validateSss(adm)
        """
        Persists `sss` ADM in database
        """
        sss_adm.saveSss(adm)

    # Enter a parse tree produced by Sssv3Parser#s1_metadata.
    def enterS1_metadata(self, ctx:Sssv3Parser.S1_metadataContext): 
        """
        Gets the `seg1 name` from the antlr generated 
        parse tree, which is further used in a program.
        """
        self.seg1_name = ctx.Meta_data().getText().lower()
        """
        Calls `setSeg1` method to create a `seg1`(Metadata) dict
        as sub dictionary inside a `sss` dict.
        Sends:`seg1 name`
        self.sss = {
                    seg1 name: {}
        }
        """
        self.sss.setSeg1(seg1=self.seg1_name)
    
    # Exit a parse tree produced by Sssv3Parser#s1_metadata.
    def exitS1_metadata(self, ctx:Sssv3Parser.S1_metadataContext):
        pass

    # Enter a parse tree produced by Sssv3Parser#md_avp.
    def enterMd_avp(self, ctx:Sssv3Parser.Md_avpContext):
        """
        Gets the list of attr, values of the `seg1`(Metadata) from the parse tree
        and saves it, which is further used in a program. 
        """
        seg1attr_list = ctx.Attr()
        seg1value_list = ctx.Value()
        """
        Loops through the `seg1`(Metadata) attr list.
        Calls `setSeg1Attr` method to populate the `seg1` dict with attr, value 
        pairs.
        Sends: `seg1 name`, `seg1 attrs`, `seg1 values`
        self.sss = {
                   seg1 name: {
                              seg1 attr : seg1 value,
                              seg1 attr : seg1 value,
                              ........
                   } 
        }
        """
        for index in range(len(seg1attr_list)):
            self.sss.setSeg1Attr(seg1=self.seg1_name, attr=seg1attr_list[index].getText().lower(), value=seg1value_list[index].getText().strip('"'))      

    # Exit a parse tree p.roduced by Sssv3Parser#md_avp.
    def exitMd_avp(self, ctx:Sssv3Parser.Md_avpContext):
        pass

    # Enter a parse tree produced by Sssv3Parser#s2_pgxform_stmnts.
    def enterS2_pgxform_stmnts(self, ctx:Sssv3Parser.S2_pgxform_stmntsContext):
        """
        Gets the `seg2 name` from the antlr generated 
        parse tree, which is further used in a program.
        """
        self.seg2_name = ctx.Pg_xform().getText().lower()
        """
        Calls `setSeg2` method to create a `seg2(pgxform)` list
        in a sss dict.
        Sends:`seg2 name`
        self.sss = {
                      seg1 name: {
                              seg1 attr : seg1 value,
                              seg1 attr : seg1 value,
                              ........
                      }
                      seg2 name : []  # Creates a list
        }
        """
        self.sss.setSeg2(seg2=self.seg2_name)

    # Exit a parse tree produced by Sssv3Parser#s2_pgxform_stmnts.
    def exitS2_pgxform_stmnts(self, ctx:Sssv3Parser.S2_pgxform_stmntsContext):
        pass

    # Enter a parse tree produced by Sssv3Parser#stmnt.
    def enterStmnt(self, ctx:Sssv3Parser.StmntContext):
        """
        Gets the name of the location from the parse tree
        and saves it, which can be further used in a program.
        """
        self.loc = ctx.Loc().getText()
        """
        Calls `setSeg2Stmnts` method to create a `seg2_stmnt` dict and
        populates the dict with `loc` and `acpair` stmnts. 
        Sends: loc name, acpairname 
        self.seg2_stmnt = {
                            loc name: {},
                            acpairname: []
        } 
        """
        self.sss.setSeg2Stmnts(loc=self.loc, acpair="acpair")
        
    # Exit a parse troduced by Sssv3Parser#stmnt.
    def exitStmnt(self, ctx:Sssv3Parser.StmntContext):
        """
        Calls `setActnCond` method to append `ac_stmnt` dict 
        in a `seg2_stmnt['acpair']` list.
        self.seg2_stmnt = {
                            loc name: {
                               loc attr : loc value,
                               loc attr : loc value,
                               ......
                            },
                            acpairname: [
                                           {
                                             action name: {
                                                 action attr: action value,
                                                 action attr: action value,
                                                 ........
                                             }   
                                           },
                                           .....
                            ]
        }                  
        """
        self.sss.setActnCond()
        """
        appends `seg2_stmnt` dict in a seg2(pgxform) list.
        Sends: seg2 name
        self.sss = {
                     seg1.....,
                     seg2 name : [
                            {  # seg2_stmnt dict
                                loc name: {
                                  loc attr : loc value,
                                  loc attr : loc value,
                                  ......
                                },
                                acpairname: [
                                      {
                                         action name:{
                                              action attr: action value,
                                              action attr: action value,
                                              ......
                                         },
                                         cond name:{
                                              cond attr: cond value,
                                              cond attr: cond value,
                                              ......                                            
                                         }   
                                      },
                                      .....
                               ]
                           },
                     ],
                     seg3.....
        } 
        """
        self.sss.setFinalSeg2(seg2=self.seg2_name)

    # Enter a parse tree produced by Sssv3Parser#ac_pair.
    def enterAc_pair(self, ctx:Sssv3Parser.Ac_pairContext):
        """
        Checks if `actions` and `cond` exists in a tree. If both exists gets the `action` 
        and `cond` text from a tree. Then calls a `setAcStmnt` method which creates a 
        ac_stmnt(dict) and populates the dict with action and cond stmnts(dict's) as a sub dict's.
        Sends: action and cond text
        self.ac_stmnt = {
                           action text: {}
                           cond text: {}
        }
        """
        if(ctx.Action() and ctx.Cond()):
            self.action = ctx.Action().getText().lower()
            self.cond = ctx.Cond().getText().lower()
            self.sss.setAcStmnt(action = self.action, cond = self.cond)
            """
            Checks if `actions` exists in a tree. If exists gets the `action` text 
            from a tree. Then calls a `setAcStmnt` method which creates a ac_stmnt(dict) 
            and populates the dict with action stmnts(dict's) as a sub dict.
            Sends: action text
            self.ac_stmnt = {
                           action text: {}
            }
            """

        elif(ctx.Action()):
            self.action = ctx.Action().getText().lower()
            self.sss.setAcStmnt(action = self.action)            

    # Exit a parse tree produced by Sssv3Parser#ac_pair.
    def exitAc_pair(self, ctx:Sssv3Parser.Ac_pairContext):
        pass

    # Enter a parse tree produced by Sssv3Parser#loc_avp.
    def enterLoc_avp(self, ctx:Sssv3Parser.Loc_avpContext):
        """
        Gets the list of attr, values of the `location` from the parse tree
        and saves it, which is further used in a program. 
        """
        loc_attr_list = ctx.Attr()
        loc_value_list = ctx.Value()
        """
        Loops through the `location` attr list.
        Calls `setLocAttr` method to populate the `loc` dict with attr, value 
        pairs. `loc` is a sub dictionary inside `seg2_stmnt` dict.
        Sends: `loc name`, `loc attrs`, `loc values`
        self.seg2_stmnt = {
                            loc name: {
                               loc attr : loc value, 
                               loc attr : loc value,
                               ......
                            },
                            acpairname: []
        }
        """
        for index in range(len(loc_attr_list)):
            self.sss.setLocAttr(loc=self.loc, attr=loc_attr_list[index].getText().lower(), value=loc_value_list[index].getText().strip('"'))        

    # Exit a parse tree produced by Sssv3Parser#loc_avp.
    def exitLoc_avp(self, ctx:Sssv3Parser.Loc_avpContext):
        pass

    # Enter a parse tree produced by Sssv3Parser#actn_avp.
    def enterActn_avp(self, ctx:Sssv3Parser.Actn_avpContext):
        """
        Gets the list of attr, values of the `actions` from the parse tree
        and saves it, which is further used in a program. 
        """
        actn_attr_list = ctx.Attr()
        actn_value_list = ctx.Value()
        """
        Loops through the `actions` attr list.
        Calls `setActionAttr` method to populate the `action` dict with attr, value 
        pairs. 
        sends: `action name`, `action attrs`, `action values`
        self.ac_stmnt = {
                          action name: {
                              action attr: action value,
                              action attr: action value,
                              ......
                         }
        }
        """
        for index in range(len(actn_attr_list)):
            self.sss.setActionAttr(action=self.action, attr=actn_attr_list[index].getText().lower(), value=actn_value_list[index].getText().strip('"'))

    # Exit a parse tree produced by Sssv3Parser#actn_avp.
    def exitActn_avp(self, ctx:Sssv3Parser.Actn_avpContext):
        pass

    # Enter a parse tree produced by Sssv3Parser#cond_avp.
    def enterCond_avp(self, ctx:Sssv3Parser.Cond_avpContext):
        """
        Gets the list of attr, values of the `cond` from the parse tree
        and saves it, which is further used in a program. 
        """
        cond_attr_list = ctx.Attr()
        cond_value_list = ctx.Value()
        """
        Loops through the `cond` attr list.
        Calls `setCondAttr` method to populate the `cond` dict with attr, value 
        pairs. 
        Sends: `cond name`, `cond attrs`, `cond values`
        self.ac_stmnt = {
                          action name: {
                             action attr: action value,
                             action attr: action value,
                             ......
                          }
                          cond name: {
                             cond attr: cond value,
                             cond attr: cond value,
                             ......
                          }
        }
        """
        for index in range(len(cond_attr_list)):
            self.sss.setCondAttr(cond=self.cond, attr=cond_attr_list[index].getText().lower(), value=cond_value_list[index].getText().strip('"'))

    # Exit a parse tree produced by Sssv3Parser#cond_avp.
    def exitCond_avp(self, ctx:Sssv3Parser.Cond_avpContext):
        pass

    # Enter a parse tree produced by Sssv3Parser#s3_ssscrite.
    def enterS3_ssscrite(self, ctx:Sssv3Parser.S3_ssscriteContext):
        """
        Gets the `seg3 name` from the antlr generated 
        parse tree, which is further used in a program.
        """
        self.seg3_name = ctx.Sss_crite().getText().lower()
        """
        Calls `setSeg3` method to create a `seg3`(ssscrite) dict
        as a sub dictionary inside a `sss` dict.
        sends:`seg3 name`
        self.sss = {
                    seg3 name: {}
        }
        """
        self.sss.setSeg3(seg3=self.seg3_name)

    # Exit a parse tree produced by Sssv3Parser#s3_ssscrite.
    def exitS3_ssscrite(self, ctx:Sssv3Parser.S3_ssscriteContext):
        pass

    # Enter a parse tree produced by Sssv3Parser#selctn_avp.
    def enterSelctn_avp(self, ctx:Sssv3Parser.Selctn_avpContext):
        """
        Gets the list of attr, values of the seg3(ssscrite) from the parse tree
        and saves it, which is further used in a program. 
        """
        seg3attr_list = ctx.Attr()
        seg3value_list = ctx.Value()
        """
        Loops through the `seg3`(ssscrite) attr list.
        Calls `setSeg3Attr` method to populate the `seg3` with attr, value 
        pairs.
        sends: `seg3 name`, `seg3 attrs`, `seg3 values`
        self.sss = {
                      seg3 name : {
                          seg3 attr : seg3 value,
                          seg3 attr : seg3 value,
                          ........
                      }
        }
        """
        for index in range(len(seg3attr_list)):
            self.sss.setSeg3Attr(seg3=self.seg3_name, attr=seg3attr_list[index].getText().lower(), value=seg3value_list[index].getText().strip('"'))

    # Exit a parse tree produced by Sssv3Parser#selctn_avp.
    def exitSelctn_avp(self, ctx:Sssv3Parser.Selctn_avpContext):
        pass


